﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;
using System;

public class WaveTimeTest {

	[Test]
	public void EditorTest() {
		//Arrange
		var gameObject = new GameObject();

        LevelManager levelManager = new LevelManager();
        levelManager.InitLevels();
        for (int i = 1; i < 25; i++)
        {
            levelManager.WaveNumber = i;
            int enemies = levelManager.GetNumberOfEnemiesForWave();
            float secondsToWave = levelManager.GetNumberOfSecondsForNextWave();
            //    Debug.LogFormat("Wave {0} time {1} enemies", i, secondsToWave, enemies);
            float enemiesPerSec = enemies / secondsToWave;
            Console.WriteLine("Wave {0} time {1} enemies {2} enemies per sec {3}", i, secondsToWave, enemies, enemiesPerSec);
        }
		//Act
		//Try to rename the GameObject
		var newGameObjectName = "My game object";
		gameObject.name = newGameObjectName;

		//Assert
		//The object has a new name
		Assert.AreEqual(newGameObjectName, gameObject.name);
	}
}
