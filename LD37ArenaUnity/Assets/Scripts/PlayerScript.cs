﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class PlayerScript : MonoBehaviour
{

    Rigidbody2D body;
    Animator animator;
    SpriteRenderer spriteRenderer;
    public GameObject Bullet;
    public GameObject FallAnimation;
    public float BulletSpeed = 4.0f;
  //  public float Health;
    public float InitialHealth = 10;
    Character characterBehaviour;
    // Use this for initialization
    LevelManager levelManager;

   // public Transform FireOffset;

    public AudioClip[] PlayerTakeDamage;
    public AudioClip[] PlayerFire;

    public bool IsDead()
    {
        return characterBehaviour.Health <= 0;
    }
    Vector2 CurrentDirection;
    void Start()
    {
        this.body = GetComponent<Rigidbody2D>();
        this.animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        this.characterBehaviour = GetComponent<Character>();
        this.characterBehaviour.SupportsTemporaryImmunity = true;
        this.characterBehaviour.DestroyOnDie = false;
        this.animator.speed = 0;
        SetInitialDirection();
        this.characterBehaviour.Health = InitialHealth;
        this.characterBehaviour.PointValue = 0;
        levelManager = FindObjectOfType(typeof(LevelManager)) as LevelManager;
        SetVectorDirectionList();
        ReDisplayHealth();


    }

    public void DoReset()
    {
        SetInitialDirection();
        this.Firing = false;
    }
    public void SetInitialDirection()
    {
        CurrentDirection = Vector2.down;
    }

    internal void AddHealth(float healthValue)
    {
        this.characterBehaviour.AddHealth(healthValue);
        ReDisplayHealth();
    }

    void SetVectorDirectionList()
    {
        vecDirs = new List<Vector2>();
        vecDirs.Add(Vector2.down);
        Vector2 dleft = (Vector2.down + Vector2.left);
        dleft.Normalize();
        vecDirs.Add(dleft);
        vecDirs.Add(Vector2.left);
        Vector2 uleft = Vector2.up + Vector2.left;
        uleft.Normalize();
        vecDirs.Add(uleft);
        vecDirs.Add(Vector2.up);
        Vector2 uright = Vector2.up + Vector2.right;
        uright.Normalize();
        vecDirs.Add(uright);
        vecDirs.Add(Vector2.right);
        Vector2 dright = Vector2.right + Vector2.down;
        dright.Normalize();
        vecDirs.Add(dright);

    }
    public float speed = 2f;
    public float minDead = 0.01f;

    int CurrentAnimationIndex = 0;
    List<Vector2> vecDirs;

    Vector2 FindClosest8WayDirectionForVector(Vector2 direction)
    {
        float max = 0;
        int selected = 0;
        for (int i = 0; i < vecDirs.Count; i++)
        {
            float dot = Vector2.Dot(vecDirs[i], direction);
            if (dot > max)
            {
                max = dot;
                selected = i;
            }

        }
        return vecDirs[selected];
    }
    void SetAnimationForCurrentDirection(Vector2 direction)
    {
      
      //  float max = 0;
      

        Vector2 currDir = FindClosest8WayDirectionForVector(direction);
        int selected  = vecDirs.IndexOf(currDir);
        //for (int i= 0;i < vecDirs.Count;i++)
        //{
        //    float dot = Vector2.Dot(vecDirs[i], direction);
        //    if (dot > max)
        //    {
        //        max = dot;
        //        selected = i;
        //    }

        //}
        if (CurrentAnimationIndex != selected)
        {
            CurrentAnimationIndex = selected;
            this.animator.SetTrigger("Reset");
            this.animator.SetInteger("Direction", CurrentAnimationIndex);
        }
        
    }
    // Update is called once per frame
    void Update()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical  = Input.GetAxis("Vertical");

        //if (Input.GetKey(KeyCode.LeftArrow))
        //{
        //    horizontal = -1.0f;
        // //   spriteRenderer.flipX = false;
        //}
        //else if (Input.GetKey(KeyCode.RightArrow))
        //{
        //  //  spriteRenderer.flipX = true;
        //    horizontal = 1.0f;
        //}
        //else
        //    horizontal = 0.0f;

        //if (Input.GetKey(KeyCode.UpArrow))
        //{
        //    vertical = 1.0f;
        //}
        //else if (Input.GetKey(KeyCode.DownArrow))
        //{
        //    vertical = -1.0f;
        //}
        //else
        //    vertical = 0.0f;

        //  Debug.LogFormat("Horizontal {0} Vertical {1}", horizontal, vertical);

        if (Mathf.Abs(horizontal) < minDead)
            horizontal = 0.0f;
            
        if (Mathf.Abs(vertical) < minDead)
            vertical = 0.0f;

        Vector2 vec = new Vector2(horizontal, vertical);
        float mag = vec.SqrMagnitude();
        if (mag < minDead)
        {
            vec = Vector2.zero;
            this.animator.speed = 0;
        }
        else
        {
            vec.Normalize();
            vec = FindClosest8WayDirectionForVector(vec);
            CurrentDirection = vec;
            SetAnimationForCurrentDirection(CurrentDirection);
            this.animator.speed = 1;
        }

        //if (this.animator.speed == 0 && mag > 0.0f)
            
        //else
        //{
           
        //}
        //if (mag > 0.0f)
        //{
        //    CurrentDirection =vec;
        //    SetAnimationForCurrentDirection(CurrentDirection);
        //}
        //if (horizontal != 0 && vertical != 0)
        //{
        //    if (this.animator.speed == 0)
        //        ;// this.animator.speed = 1.0f;
        //}
        //else
        //    ;//  this.animator.speed = 0;

    //    if (horizontal == 0 && vertical == 0)
   //         this.animator.speed = 0;
        this.body.velocity = vec * speed;
        //if (horizontal > minDead)
        //{
        //    this.body.velocity = Vector2.right * speed;

        //    this.animator.speed = 1;
        //}
        //else if (horizontal < -minDead)
        //{
        //    this.body.velocity = Vector2.left * speed;

        //    this.animator.speed = 1;
        //}
        //else
        //{
        //    this.body.velocity = Vector2.zero;
        //    this.animator.speed = 0;
        //}

        if (IsFireButtonDown())
            _trying_to_fire = true;
        else
            _trying_to_fire = false;
        TryFire();

        if (this.characterBehaviour.Immune)
        {
            this.spriteRenderer.color = Color.magenta;
        }
        else
            this.spriteRenderer.color = Color.white;
    }

    bool Firing;

    private void TryFire()
    {
        if (!this.Firing && _trying_to_fire)
        {
            StartCoroutine(FireReset());
        }
    }

    public void RunTemporaryImmunity()
    {
        this.characterBehaviour.RunTemporaryImmunity();
    }
    public float FireResetTime = 1.0f; 
    IEnumerator FireReset()
    {
        Firing = true;
        DoFire();
        yield return new WaitForSeconds(FireResetTime);
        Firing = false;
    }

    bool _trying_to_fire;
    bool IsFireButtonDown()
    {
        return Input.GetKeyDown(KeyCode.Space) || Input.GetButtonDown("Fire1");
    }


    //public void OnTriggerEnter2D(Collider2D collision)
    //{
    //    Debug.Log("Hit OnTriggerEnter2D");
    //    if (collision.gameObject.tag == "Player")
    //        return;
    //    //if (collision.gameObject.tag == "AddHealth")
    //    //{
    //    //    AddHealth health = collision.gameObject.GetComponent<AddHealth>();
    //    //    if (health != null)
    //    //    {
    //    //        this.characterBehaviour.AddHealth(health);
    //    //        ReDisplayHealth();

    //    //    }
            
    //    //}
    //    if (collision.gameObject.tag == "Enemy" || collision.gameObject.tag == "EnemyProjectile")
    //    {
    //        TryTakeDamage(collision.gameObject);
    //        //var enemy = collision.gameObject.GetComponent<Enemy>();
    //        //if (enemy != null)
    //        //{
                
    //        //    this.characterBehaviour.TakeDamage(enemy.DamageValue);
    //        //}
    //    }
       
    //}

    public void TryTakeDamage(GameObject fromObject)
    {
        var dmg = fromObject.GetComponent<InflictDamage>();
        if (dmg != null)
        {
            if (!this.characterBehaviour.Immune)
                levelManager.PlayOneShot(PlayerTakeDamage);
                //levelManager.PlayPlayerTakeDamage();
            this.characterBehaviour.TakeDamage(dmg.DamageValue,true);
            ReDisplayHealth();
        }
    }

  
    public void PlayFallAnimation(Vector3 position)
    {
      //  this.animator.SetTrigger("Reset");
       // this.animator.SetBool("DoReset", true);
       // this.animator.SetTrigger("DoFall");

        GameObject scorps = FallAnimation;
        Instantiate(scorps, position, Quaternion.identity);
    }

    public float FallOffsetDistance = 0.5f;
    public void TakeFall(GameObject fromObject)
    {
        var dmg = fromObject.GetComponent<InflictDamage>();
        //Debug.Log("Called TakeFall");
        if (dmg != null)
        {
            //if (!this.characterBehaviour.Immune)
             //   levelManager.PlayPlayerTakeDamage();
            this.characterBehaviour.Immune = false;
            this.characterBehaviour.TakeDamage(dmg.DamageValue,false);
            ReDisplayHealth();

        }
        Vector3 fallObjectDir = (fromObject.transform.position - this.transform.position);
        fallObjectDir.Normalize();

        Vector3 fallPos = this.transform.position + (FallOffsetDistance * fallObjectDir);

        PlayFallAnimation(fallPos);
        if (!IsDead())
            levelManager.DoPlayerReset(this.gameObject);
       
    }


    void ReDisplayHealth()
    {
        this.levelManager.ShowPlayerHealth((int)this.characterBehaviour.Health);
    }
    
    public void OnCollisionStay2D(Collision2D collision)
    {
        //Debug.Log("Hit OnCollisionEnter2D");
        if (collision.gameObject.tag == "Player")
            return;
        if (collision.gameObject.tag == "Enemy")
        {
            TryTakeDamage(collision.gameObject);
            //var enemy = collision.gameObject.GetComponent<Enemy>();
            //if (enemy != null)
            //{

            //    this.characterBehaviour.TakeDamage(enemy.DamageValue);
            //}
        }

    }


    public Vector3 FireOffset = new Vector3(0, -0.25f,0f);
    void DoFire()
    {
      //  levelManager.PlayPlayerFire();
        levelManager.PlayOneShot(PlayerFire,false);
        Vector3 postion = this.transform.position + FireOffset;

        Quaternion rotation = Quaternion.identity;
        //if (_current_direction == Direction.North)

        //    rotation = Quaternion.identity;
        //if (_current_direction == Direction.South)

        //    rotation = Quaternion.AngleAxis(180, Vector3.forward);

        //if (_current_direction == Direction.West)
        //    rotation = Quaternion.AngleAxis(90, Vector3.forward);

        //if (_current_direction == Direction.East)
        //    rotation = Quaternion.AngleAxis(270, Vector3.forward);


        GameObject bullet = Instantiate(Bullet, postion, rotation) as GameObject;
        Rigidbody2D bullet_body = bullet.GetComponent<Rigidbody2D>();
        this.CurrentDirection.Normalize();
        bullet_body.velocity = CurrentDirection * BulletSpeed;// VectorForDirection(_current_direction) * BulletSpeed;
        float smallAngle = Vector2.Angle(Vector2.up, CurrentDirection);
        if (CurrentDirection.x > 0)
          smallAngle =  360 -smallAngle;
      
        bullet_body.rotation = smallAngle;//Quaternion.FromToRotation(Vector3.up, CurrentDirection).;
       // SoundManager.instance.PlaySingleWithRandomPitch(this.fireSound);
    }
}
