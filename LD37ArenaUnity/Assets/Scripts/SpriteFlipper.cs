﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteFlipper : MonoBehaviour {
    public bool InvertX = false;
    public bool InvertY = false;
    SpriteRenderer spriteRenderer;
    // Use this for initialization
    void Start () {
        spriteRenderer = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
        if (InvertX && !this.spriteRenderer.flipX)
            this.spriteRenderer.flipX = true;

    }
}
