﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmpBossBehaviour : MonoBehaviour, IDamageSubscriber
{


    float WalkSpeed = 1.5f;
    LevelManager levelManager;
    Rigidbody2D body;
    Animator animator;
    public float DamageValue = 5.0f;
    Action CurrentAction;
    public GameObject BulletPreFab;
    public float BulletSpeed = 3.0f;
    bool JustShot = false;
    enum Action { None, Taunt, Attack, Walk };
    //public GameObject BossHealthBar;
    Character character;
    HealthBar healthbar;

    public AudioClip[] FireClip;
    SpriteRenderer spriteRenderer;
    // Use this for initialization
    void Start()
    {
        levelManager = FindObjectOfType(typeof(LevelManager)) as LevelManager;
        healthbar = FindObjectOfType(typeof(HealthBar)) as HealthBar;
        this.animator = GetComponent<Animator>();
        this.body = GetComponent<Rigidbody2D>();
        //   this.healthbar = BossHealthBar.GetComponent<HealthBar>();
        this.character = GetComponent<Character>();
        this.character.DamageSubscriber = this;
        this.spriteRenderer = GetComponent<SpriteRenderer>();
        StartWalk();

    }

    Action GetRandomAction()
    {
        //return Action.Walk;
        float rand = UnityEngine.Random.Range(0, 100);
        if (0 < rand && rand < 20)
        {
            return Action.Taunt;
        }
        if (20 < rand && rand < 60)
        {
            return Action.Walk;
        }
        if (60 < rand && rand < 100 && !JustShot)
        {
            return Action.Attack;
        }
        JustShot = false;
        return Action.Walk;
    }

    IEnumerator SwitchAction(float time)
    {
        yield return new WaitForSeconds(time);
        //Debug.Log("Called Switch Action");
        this.CurrentAction = Action.None;// GetRandomAction();

        //    AddLionEnemy(1);
        //AddLionEnemy(2);
        //AddLionEnemy(3);
        //AddLionEnemy(4);
        //AddLionEnemy(5);
        //AddLionEnemy(6);
        //AddLionEnemy(7);
        //AddLionEnemy(8);
    }

    void StartTaunt()
    {
        this.body.velocity = Vector2.zero;
      //  levelManager.PlayEnemyTaunt();
        this.animator.SetBool("IsWalking", false);
        this.animator.SetTrigger("DoTaunt");
        StartCoroutine("SwitchAction", 1);
    }
    // Update is called once per frame
    public float AttackLaunchTime = 0.5f;
    IEnumerator FinishAttack()
    {
        yield return new WaitForSeconds(AttackLaunchTime);
        DoFire();
        JustShot = true;
        StartCoroutine("SwitchAction", 2.0f);
    }
    void StartAttack()
    {
        this.animator.SetBool("IsWalking", false);
        this.body.velocity = Vector2.zero;
        this.animator.SetTrigger("DoAction");
        StartCoroutine("FinishAttack");

    }

    public bool IsDead()
    {
        return this.character.IsDead();
    }
    void DoFire()
    {

        if (!levelManager.IsPLayerAlive())
            return;
        levelManager.PlayOneShot(FireClip);
        Vector3 postion = this.transform.position + new Vector3(0.5f, 0.5f, 0);

        Quaternion rotation = Quaternion.identity;
       Vector2 start = Vector2.up;
        int number;
        if (UnityEngine.Random.Range(0, 1) < 0.5f || this.character.Health < this.character.MaximumHealth / 2.0f)
            number = 16;
        else
            number = 8;
      
        for (int i=0;i < number;i++)
        {
            GameObject bullet = Instantiate(BulletPreFab, postion, rotation) as GameObject;
            Rigidbody2D bullet_body = bullet.GetComponent<Rigidbody2D>();
            start  = Quaternion.Euler(0, 0, 360.0f / number) * start;
            Vector2 fireDirn = start;//= levelManager.GetPlayerPosition() - body.position;
            fireDirn.Normalize();
            //     this.CurrentDirection.Normalize();
            bullet_body.velocity = fireDirn * BulletSpeed;// VectorForDirection(_current_direction) * BulletSpeed;
        }
        

        
        //float smallAngle = Vector2.Angle(Vector2.up, fireDirn);
        //if (fireDirn.x > 0)
        //    smallAngle = 360 - smallAngle;

        //bullet_body.rotation = smallAngle;//Quaternion.FromToRotation(Vector3.up, CurrentDirection).;
        //                                  // SoundManager.instance.PlaySingleWithRandomPitch(this.fireSound);

    }


    public void OnTriggerEnter2D(Collider2D collision)
    {
        //Debug.Log("Hit OnTriggerEnter2D");
        if (collision.gameObject.tag == "Player")
        {

            return;
        }
        if (collision.gameObject.tag == "Enemy")
        {
            var enemy = collision.gameObject.GetComponent<Enemy>();
            if (enemy != null)
            {

                ;// this.characterBehaviour.TakeDamage(enemy.DamageValue);
            }
        }

    }


    public void OnCollisionStay2D(Collision2D collision)
    {
        //Debug.Log("Hit OnCollisionEnter2D");
        if (collision.gameObject.tag == "Player")
            return;
        if (collision.gameObject.tag == "Enemy")
        {
            var enemy = collision.gameObject.GetComponent<Enemy>();
            if (enemy != null)
            {

                ;//   this.characterBehaviour.TakeDamage(enemy.DamageValue);
            }
        }

    }

    void SetNewAction()
    {
        CurrentAction = GetRandomAction();
        //Debug.LogFormat("Called Set new Action Got {0}", CurrentAction.ToString());
        if (CurrentAction == Action.Walk)
        {
            StartWalk();

        }
        if (CurrentAction == Action.Taunt)
        {
            StartTaunt();
        }
        if (CurrentAction == Action.Attack)
        {
            StartAttack();

        }
    }

    void Update()
    {
        if (CurrentAction == Action.None)
        {
            SetNewAction();
        }
        if (CurrentAction == Action.Walk)
        {
            DoWalk();
        }
        else if (CurrentAction == Action.Taunt)
        {

        }

        if (!this.levelManager.IsPLayerAlive())
        {
            this.body.velocity = Vector2.zero;
            return;
        }



    }

    Vector2 TargetWalk;
    void StartWalk()
    {
        this.animator.SetBool("IsWalking", true);
        Vector2 playerPos;
        if (this.levelManager.IsPLayerAlive())
        {
            playerPos = this.levelManager.GetPlayerPosition();
        }
        else
            playerPos = this.levelManager.PlayerStart.position;
        this.TargetWalk = playerPos;
        StartCoroutine("SwitchAction", 5.0f);
    }
    void DoWalk()
    {
        Vector2 dirn = TargetWalk - this.body.position;
        dirn.Normalize();
        this.body.velocity = dirn * WalkSpeed;
    }

    public float DamageFlashTime = 0.3f;
    public Color DamageColor;
    IEnumerator ResetAfterDamage()
    {
        yield return new WaitForSeconds(DamageFlashTime);
        this.spriteRenderer.color = Color.white;
    }

    public void TakeDamage(float damage)
    {
        var character = this.GetComponent<Character>();
        float fullHealth = character.MaximumHealth;
        float perc = character.Health / fullHealth;
        this.healthbar.SetValue(perc);
        this.spriteRenderer.color = DamageColor;
        StartCoroutine("ResetAfterDamage");
    }
}


