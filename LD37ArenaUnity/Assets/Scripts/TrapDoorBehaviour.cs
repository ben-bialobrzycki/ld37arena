﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapDoorBehaviour : MonoBehaviour {

    public float CloseTime = 0.5f;
    public float OpenTime = 0.5f;
    public float AnimatorSpeed = 0.7f;
    SpriteRenderer renderer;

    BoxCollider2D collider;

    [HideInInspector]
    public bool IsOpen;
    Animator animator;
    // Use this for initialization

    bool isClosing;
    IEnumerator CloseGate()
    {
        this.animator.speed = AnimatorSpeed;
        this.animator.SetBool("DoOpen", false);
        this.animator.SetBool("DoClose", true);
        yield return new WaitForSeconds(CloseTime);
        IsOpen = false;
        collider.enabled = false;

    }

    //float DamageValue = 2;
    //public void OnTriggerStay2D(Collider2D collision)
    //{
    //      Debug.LogFormat("Called On Trigger Enter this {0} Collider2d {1}", this.gameObject.tag, collision.gameObject.tag);
       
    //    if (collision.gameObject.tag == "Enemy" )
    //    {
    //        var enemy = collision.gameObject.GetComponent<Character>();
    //        if (enemy != null)
    //        {
    //            enemy.TakeDamage(this.GetComponent<InflictDamage>().DamageValue);
    //        }
    //    }
    //    if (collision.gameObject.tag == "Player")
    //    {
    //        var player = collision.gameObject.GetComponent<PlayerScript>();
    //        if (player != null)
    //        {
    //            player.TryTakeDamage (this.gameObject);
    //        }
    //    }

       
    //}

    public void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.LogFormat("Called On Trigger Enter this {0} Collider2d {1}", this.gameObject.tag, collision.gameObject.tag);

        //if (collision.gameObject.tag == "Enemy")
        //{
        //    var enemy = collision.gameObject.GetComponent<Character>();
        //    if (enemy != null)
        //    {
        //        enemy.TakeDamage(this.GetComponent<InflictDamage>().DamageValue);
        //    }
        //}
        if (collision.gameObject.tag == "Player")
        {
            var player = collision.gameObject.GetComponent<PlayerScript>();
            if (player != null)
            {
                player.TakeFall(this.gameObject);
            }
        }


    }


    public void StartClose()
    {
        StartCoroutine("CloseGate");
    }

    void OpenGate()
    {

    }


    IEnumerator FinishOpen()
    {

        yield return new WaitForSeconds(OpenTime);
        IsOpen = true;
        collider.enabled = true;
    }


    public void OpenAndEnterObject()
    {
        //this.animator.
        this.animator.speed = AnimatorSpeed;
        this.animator.SetBool("DoOpen", true);
        this.animator.SetBool("DoClose", false);
        if (!isClosing)
        {
            StartCoroutine("FinishOpen");
        }
        
    }



    void Start()
    {
        this.collider = GetComponent<BoxCollider2D>();
        this.animator = GetComponent<Animator>();
        this.renderer = GetComponent<SpriteRenderer>();
        //    this.animator.GetComponent<Animation>("")
        this.animator.speed = 0;
        this.collider.enabled = IsOpen;

    }
    // Update is called once per frame
    void Update () {
        //if (IsOpen)
        //    renderer.color = Color.red;
        //else
        //    renderer.color = Color.white;

    }
}
