﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public float DamageValue = 1.0f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public bool IsDestroyedByPlayerProjectile;

    public void OnTriggerEnter2D(Collider2D collision)
    {
        string thisTag = this.gameObject.tag;
        string otherTag = collision.gameObject.tag;
      //  Debug.LogFormat("Called On Trigger Enter this {0} Collider2d {1}", this.gameObject.tag, collision.gameObject.tag);
        if (otherTag == "Player" && thisTag == "PlayerProjectile")
            return;
        if (otherTag == "EnemyProjectile" && thisTag == "EnemyProjectile")
            return;
        if (otherTag == "Enemy" && thisTag == "PlayerProjectile")
        {
            var enemy = collision.gameObject.GetComponent<Character>();
            if (enemy != null)
            {
                enemy.TakeDamage(this.DamageValue,true);
            }
        }

     
        if (otherTag == "Player" && thisTag == "EnemyProjectile")
        {
            var player = collision.gameObject.GetComponent<PlayerScript>();
            if (player != null)
            {
                player.TryTakeDamage(this.gameObject);
            }
        }

        if (thisTag == "EnemyProjectile" && otherTag == "PlayerProjectile" && !IsDestroyedByPlayerProjectile)
            return;

        Destroy(this.gameObject);
    }

    public void OnCollisionStay2D(Collision2D collision)
    {
     //   Debug.Log("Hit OnCollisionStay2D");
        //if (collision.gameObject.tag == "Player")
        //    return;
        //if (collision.gameObject.tag == "Enemy")
        //{
        //    var enemy = collision.gameObject.GetComponent<Enemy>();
        //    if (enemy != null)
        //    {

        //        this.characterBehaviour.TakeDamage(enemy.DamageValue);
        //    }
        //}

    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
     //   Debug.Log("Hit OnCollisionEnter2D");
        //if (collision.gameObject.tag == "Player")
        //    return;
        //if (collision.gameObject.tag == "Enemy")
        //{
        //    var enemy = collision.gameObject.GetComponent<Enemy>();
        //    if (enemy != null)
        //    {

        //        this.characterBehaviour.TakeDamage(enemy.DamageValue);
        //    }
        //}

    }
}
