﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateEnter : MonoBehaviour {

    public float GateWaitOpen = 3;
    public float EnemyWait = 2;

    Animator animator;
    // Use this for initialization

    void Start()
    {
        this.animator = GetComponent<Animator>();
    //    this.animator.GetComponent<Animation>("")
        this.animator.speed = 0;
   
    }


    IEnumerator CreateEnemy(GameObject enemyObject)
    {

        yield return new WaitForSeconds(EnemyWait);
        Instantiate(enemyObject, this.transform.position, Quaternion.identity);

    }

    bool isClosing = false;
    IEnumerator CloseGate()
    {
 
        isClosing = true;
        yield return new WaitForSeconds(GateWaitOpen);
        isClosing = false;
        this.animator.SetBool("DoOpen", false);
        this.animator.SetBool("DoClose", true);

    }

    public void OpenAndEnterObject(GameObject enemyObject)
    {
        //this.animator.
        this.animator.speed = 1;
        this.animator.SetBool("DoOpen", true);
        this.animator.SetBool("DoClose", false);
        if (!isClosing)
        {
            StartCoroutine("CloseGate");
        }
        StartCoroutine("CreateEnemy",enemyObject);
    }



	
	// Update is called once per frame
	void Update () {
   //     if (this.animator.st)
		
	}
}
