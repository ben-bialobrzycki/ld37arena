﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {

    public GameObject BackgroundImageObject;
    public GameObject FrontImageObject;

    public RectTransform BackgroundRect;
    public RectTransform FrontRect;

    Image BackgroundImage;
    Image FrontImage;
    // Use this for initialization
    void Start () {
        //BackgroundImage = BackgroundImageObject.GetComponent<Image>();
        //FrontImage = FrontImageObject.GetComponent<Image>();

        //BackgroundRect = BackgroundImageObject.GetComponent<RectTransform>();
        //FrontRect = FrontImageObject.GetComponent<RectTransform>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetValue(float percentage)
    {
        // float width = BackgroundRect.sizeDelta.x * (percentage/100.0f);
        //BackgroundRect.r
        // FrontRect.sizeDelta = new Vector2(width, FrontRect.sizeDelta.y);
        //Debug.LogFormat("Called Health Bar SetValue with perc {0}", percentage);
        FrontRect.localScale = new Vector3(  percentage , FrontRect.localScale.y, FrontRect.localScale .z);
    }
}
