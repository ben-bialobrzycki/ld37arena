﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour {

    public float Health = 10;
    public int PointValue = 10;
    LevelManager levelManager;
    [HideInInspector]
    public bool Immune;

    [HideInInspector]
    public IDamageSubscriber DamageSubscriber;

    public bool SupportsTemporaryImmunity = false;
    public bool DestroyOnDie = true;
    public float LeaveHelthChance = 0.0f;
    public float MaximumHealth;
	// Use this for initialization
	void Start () {
        levelManager = FindObjectOfType(typeof(LevelManager)) as LevelManager;
        this.Health = MaximumHealth;

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void CreateExplosion()
    {
        GameObject scorps =  levelManager.ExplosionPreFab;
        Instantiate(scorps, this.transform.position, Quaternion.identity);

    }

    public bool IsDead()
    {
        return Health <= 0;
    }

    public void TakeDamage(float damageValue, bool DoExplosion)
    {
      //  Debug.LogFormat("Called Characer Take Damage on gameObject {0}", this.gameObject.tag);
        if (Immune)
            return;
        this.Health -= damageValue;
   //     Debug.LogFormat("Characer Lost Health {0} damage Value {1} Health afterwards {2}", this.gameObject.tag,damageValue,this.Health);
        //Debug.LogFormat("Took Damage");
        if (this.Health <= 0)
            RunDie(DoExplosion);
        else
        {
            if (DamageSubscriber != null)
                DamageSubscriber.TakeDamage(damageValue);
            RunTemporaryImmunity();
        }
       
    }

    float ImmunityResetTime = 1.0f;
    IEnumerator ImmunityReset()
    {
        //Debug.Log("Set temporary immunity to true");
        Immune = true;
     
        yield return new WaitForSeconds(ImmunityResetTime);
     //   Debug.Log("ReSet temporary immunity to false");
        Immune = false;
    }

    public float PickupDropTime = 1.0f;
    void LeavePickup()
    {
       
    //    yield return new WaitForSeconds(PickupDropTime);
        //Debug.Log("Called Leave Pickup");
        GameObject scorps = levelManager.HealthPickUpPreFab;
        Instantiate(scorps, this.transform.position, Quaternion.identity);
    }


    public void RunTemporaryImmunity()
    {
        if (SupportsTemporaryImmunity)
        {
            StartCoroutine("ImmunityReset");
        }
    }
    public void RunDie(bool DoExplosion)
    {
        levelManager.UpdateScore(this.PointValue);
        if (DoExplosion)
        {
            var specialExplosion = this.GetComponent<SpecialExplosion>();
            if (specialExplosion != null)
                specialExplosion.RunExplosion();
            else
            {
                //Debug.Log("No Special Explosion");
                CreateExplosion();
            }
        }
        if (UnityEngine.Random.Range(0,100) < LeaveHelthChance)
        {
            LeavePickup();
            //StartCoroutine("LeavePickup");
        }
        this.gameObject.SetActive(false);
        if (DestroyOnDie)
            Destroy(this.gameObject);
    }

    internal void AddHealth(float healthValue)
    {
        this.Health += healthValue;
        if (this.Health > MaximumHealth)
            this.Health = MaximumHealth;
    }
}
