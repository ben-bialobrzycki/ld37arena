﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombBehaviour : MonoBehaviour {

    public GameObject BombShadowPreFab;
    public GameObject BombSpritePreFab;
    public GameObject ExplosionPreFab;


    GameObject BombShadow;
     GameObject BombSprite;


    Rigidbody2D body;
    Rigidbody2D bombSpriteBody;

    public float TimeToExplode = 3.0f;
    // Use this for initialization
    void Start () {
        this.body = GetComponent<Rigidbody2D>();
      
		
	}

    IEnumerator RunExplode()
    {
        yield return new WaitForSeconds(TimeToExplode);
        Instantiate(ExplosionPreFab, BombSprite.transform.position,Quaternion.identity);
        Destroy(this.gameObject);

    }

    Vector2 Target;
    float speed = 5.0f;
    public void ThrowToTarget(Vector2 target)
    {
        this.body = GetComponent<Rigidbody2D>();
        this.Target = target;
        this.gameObject.SetActive(true);
        BombShadow = Instantiate(BombShadowPreFab, this.transform);
        BombSprite = Instantiate(BombSpritePreFab, this.transform);
        float time = GetETA();
        VelocityYInitial =( 0.5f * AccelerationDueToGravity * time);
        VelocityY = VelocityYInitial;
    //    Debug.LogFormat("Velocity y {0} time {1}", VelocityYInitial,time);
        //   this.BombSprite.GetComponent<Rigidbody2D>()
        Throwing = true;
        StartCoroutine("RunExplode");
    }

    float VelocityYInitial;
    float VelocityY;

    bool SetToExplode = false;

    bool Throwing = false;
    float TimeTook;
    //v = 1/2 * a * t
    float CloseEnough = 0.05f;
	// Update is called once per frame
	void Update () {
        float distance = Vector2.Distance(this.body.position, this.Target);
        if (Throwing)
            TimeTook += Time.deltaTime;
        if ( distance < CloseEnough)
        {
            this.body.velocity = Vector2.zero;
         //   if (Throwing)
         //       Debug.LogFormat("Actual Time was {0} Final VelocityY {1}", TimeTook,VelocityY);
            Throwing = false;
            return;

        }

        
        VelocityY = VelocityY - (AccelerationDueToGravity * Time.deltaTime);
      //  Debug.LogFormat("Current VelocityY {0}", VelocityY);
        Vector3 oldPos  = BombSprite.transform.position;
        oldPos.y = oldPos.y + (VelocityY * Time.deltaTime);
        BombSprite.transform.position = oldPos;



    }

    public float AccelerationDueToGravity;
    float GetETA()
    {
        float distance = Vector2.Distance(this.transform.position, this.Target);
        Vector2 dirn = Target - body.position;
        dirn.Normalize();
        body.velocity = dirn * speed;
        float t = distance/speed;
        return t;
    }
}
