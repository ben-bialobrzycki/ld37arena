﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour , IDamageSubscriber
{

    float WalkSpeed = 1.5f;
    LevelManager levelManager;
    Rigidbody2D body;
    public float DamageValue = 5.0f;
    Character characterBehaviour;
    public float InitialHealth = 7.0f;
    SpriteRenderer spriteRenderer;
    //public float LeaveHealthChanceHealth = 7.0f;

    // Use this for initialization
    void Start()
    {
        levelManager = FindObjectOfType(typeof(LevelManager)) as LevelManager;
        this.body = GetComponent<Rigidbody2D>();
        this.characterBehaviour = GetComponent<Character>();
        this.characterBehaviour.Health = InitialHealth;
        this.characterBehaviour.PointValue = 5;
        this.characterBehaviour.DamageSubscriber = this;
        this.spriteRenderer = GetComponent<SpriteRenderer>();

    }

    // Update is called once per frame
    void Update () {
        if (!this.levelManager.IsPLayerAlive())
        {
            this.body.velocity = Vector2.zero;
        }
        else
        {
            Vector2 playerPos = this.levelManager.GetPlayerPosition();
            Vector2 dirn = playerPos - this.body.position;
            dirn.Normalize();
            this.body.velocity = dirn * WalkSpeed;
        }
	}

    public float DamageFlashTime = 0.3f;
    public Color DamageColor;
    IEnumerator ResetAfterDamage()
    {
        yield return new WaitForSeconds(DamageFlashTime);
        this.spriteRenderer.color = Color.white;
    }
    public void TakeDamage(float damage)
    {
        this.spriteRenderer.color = DamageColor;
        StartCoroutine("ResetAfterDamage");
    }
}
