﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthDisplay : MonoBehaviour {

    HealthHeart Health1;
    HealthHeart Health2;
    HealthHeart Health3;

    public GameObject HalfHeartImage;
    public GameObject FullHeartImage;

    // Use this for initialization
    void Start () {
        
        var children = GetComponentsInChildren<HealthHeart>();
        foreach (var go in children)
        {
            if (go.Position  == 1)
                Health1 = go;
            if (go.Position == 2)
                Health2 = go;
            if (go.Position == 3)
                Health3 = go;
        }
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void RedisplayHealth(int health)
    {
       

            Health1.Display(health );
        
       
            Health2.Display(health - 2);
     
            Health3.Display(health - 4);
        
    }
}
