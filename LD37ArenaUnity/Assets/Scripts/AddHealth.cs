﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddHealth : MonoBehaviour {

    public float HealthValue = 1;
    public float AppearDelay = 1.0f;
    public float TimeToDissapear = 4.0f;
    public AudioClip PickUpSound;
    LevelManager levelManager;
    // Use this for initialization
    void Start () {
		if (AppearDelay > 0)
        {
            ;
        }
        levelManager = FindObjectOfType(typeof(LevelManager)) as LevelManager;
        StartCoroutine("StartFade");
	}
	

    IEnumerator StartFade()
    {
        yield return new WaitForSeconds(TimeToDissapear / 2.0f);
        this.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.7f);

        yield return new WaitForSeconds(TimeToDissapear / 2.0f);
        Destroy(this.gameObject);
    }
	// Update is called once per frame
	void Update () {
		
	}

    public void OnTriggerEnter2D(Collider2D collision)
    {
         // Debug.LogFormat("Called On Trigger Enter this {0} Collider2d {1}", this.gameObject.tag, collision.gameObject.tag);
        if (collision.gameObject.tag != "Player")
            return;
    
        if (collision.gameObject.tag == "Player")
        {
            var player = collision.gameObject.GetComponent<PlayerScript>();
            if (player != null)
            {
                player.AddHealth(this.HealthValue);
                levelManager.PlayOneShot(this.PickUpSound, false);
            }
        }

        Destroy(this.gameObject);
    }
}
