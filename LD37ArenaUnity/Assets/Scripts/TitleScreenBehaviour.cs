﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TitleScreenBehaviour : MonoBehaviour {

    public GameObject[] MenuItems;
    List<Text> TextUIItems;
    int selectedItem = 0;

    Color SelectedColor;
    Color DeSelectedColor;
    bool runningGame = false;
    // Use this for initialization
    void Start () {
        runningGame = false;
   //     Debug.Log("Trying to run Start on Title Screen");
        TextUIItems = new List<Text>();
        for (var i = 0; i < MenuItems.Length; i++)
        {
            Text t = MenuItems[i].GetComponent<Text>();
           // t.color = new Color(t.color.r, t.color.g, t.color.b, 0f);
            TextUIItems.Add(t);
        }
        if (TextUIItems.Count > 1)
        {
            SelectedColor = TextUIItems[0].color;
            DeSelectedColor = TextUIItems[1].color;
        }
    }
	
    void SetSelectedOption()
    {
        //Debug.LogFormat("Selected Item is {0}", selectedItem);
        for (var i = 0; i < TextUIItems.Count; i++)
        {
            if (selectedItem == i)
                TextUIItems[i].color = SelectedColor;
            else
                TextUIItems[i].color = DeSelectedColor;

        }
    }
    float Dead = 0.05f;
    public float ControlRestTime = 0.4f;
    bool IsControlReset = false;
	// Update is called once per frame
	void Update () {
      
        if (IsFireButtonDown())
        {
#if UNITY_WEBGL
            RunGame();
#endif
            if (selectedItem == 0)
                RunGame();
            if (selectedItem == 1)
                Application.Quit();
        }

#if UNITY_WEBGL
        return;
#endif
            
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();

        
        float vert = Input.GetAxis("Vertical");
        if (Mathf.Abs(vert) <= Dead || IsControlReset)
            return;

        if (vert > 0 && selectedItem > 0)
            selectedItem--;
        else if (vert < 0 && selectedItem < TextUIItems.Count - 1)
            selectedItem++;

        SetSelectedOption();
        StartCoroutine("ControlReset");

    }

    IEnumerator ControlReset()
    {
        IsControlReset = true;
        yield return new WaitForSeconds(ControlRestTime);
        IsControlReset = false;

    }

    bool IsFireButtonDown()
    {
        return Input.GetKeyDown(KeyCode.Space) || Input.GetButtonDown("Fire1");

    }

    
    void RunGame()
    {
        if (runningGame)
            return;
        Debug.Log("Trying to run game");
        SceneManager.LoadScene("MainScene");
        runningGame = true;
    }
}
