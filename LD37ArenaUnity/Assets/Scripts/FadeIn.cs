﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeIn : MonoBehaviour {

    float alpha;
    Image image;
    RectTransform rect;
    public float FadeinTime= 6.0f;

    public float ZoomAmount = 5.0f;
    public float ZoomSpeed = 20.0f;
    float increment = 0.05f;
    float zoomincrement;
	// Use this for initialization
	void Start () {
        image = GetComponent<Image>();
        rect = GetComponent<RectTransform>();
        if (image != null)
        {
            alpha = 0;
            SetColor();
            StartCoroutine("RunFadeIn");
            StartCoroutine("RunZoom");
        }
        increment = TimeFactor * (1 / FadeinTime);
        zoomincrement = TimeFactor * ZoomSpeed;
	}

    float TimeFactor = 0.01f;
    IEnumerator RunFadeIn()
    {
        while (alpha <= 1.0f)
        {
         
            alpha += increment;
            SetColor();
            yield return new WaitForSeconds( TimeFactor );
        }
    }

    IEnumerator RunZoom()
    {
        while (rect.localScale.x  <= ZoomAmount)
        {

            rect.localScale += new Vector3(zoomincrement, zoomincrement, zoomincrement);
            yield return new WaitForSeconds(TimeFactor);
        }
    }

    void SetColor()
    {
        image.color = new Color(image.color.r, image.color.g, image.color.b, alpha);
    }
	// Update is called once per frame
	void Update () {
		
	}
}
